FROM node:7.6.0
COPY . /usr/src/app
WORKDIR /usr/src/app
RUN npm install
ENV NODE_ENV production
EXPOSE 9001
RUN npm run build
CMD npm run prod:server
