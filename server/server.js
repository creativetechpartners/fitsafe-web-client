import Webpack from 'webpack';
import WebpackDevServer from 'webpack-dev-server';
import Koa from 'koa';
import send from 'koa-send';
import serve from 'koa-static';
import path from 'path';
import WebpackConfig from '../internals/webpack/webpack.dev.babel';
import config from '../app/config';

const staticFolder = process.env.NODE_ENV === 'production'
  ? 'build'
  : 'static';

const {
  hotLoader: {
    host: hotHost,
    port: hotPort,
  },
  host,
  port,
} = config;

const app = new Koa();

app.use(serve(path.resolve(__dirname, `../${staticFolder}`)));

app.use(async (ctx) => {
  await send(ctx, `/${staticFolder}/index.html`);
});

app.listen(port, () => console.log(`Server listen on ${port}`)); // eslint-disable-line no-console

if (process.env.NODE_ENV !== 'production') {
  const devServer = new WebpackDevServer(Webpack(WebpackConfig), {
    noInfo: true,
    hot: true,
    stats: { color: true },
    proxy: { '*': `http://${host}:${port}` },
    publicPath: WebpackConfig.output.publicPath,
  });

  devServer.listen(hotPort, (err) => {
    if (err) {
      console.error(err); // eslint-disable-line no-console
    } else {
      console.log(`Hot Loader serves on http://${hotHost}:${hotPort}`); // eslint-disable-line no-console
    }
  });
}

