import 'babel-polyfill';

import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { browserHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';
import configureStore from 'store';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap-material-design/dist/css/bootstrap-material-design.min.css';
import 'bootstrap-material-design-icons/css/material-icons.min.css';
import 'whatwg-fetch';

import MainRouter from './routing';
import { setToken } from './utils/api';

setToken(window.localStorage.getItem('token'));

const store = configureStore(browserHistory);
const syncedHistory = syncHistoryWithStore(browserHistory, store);

render(
  <Provider store={store}>
    <MainRouter onUpdate={() => window.scrollTo(0, 0)} history={syncedHistory} store={store} />
  </Provider>,
  document.getElementById('root'),
);
