import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import { reducer as form } from 'redux-form';
import dashboard from 'containers/Dashboard/reducer';
import category from 'containers/Category/reducer';
import registrationErrors from 'containers/Registration/reducer';
import login from 'containers/Login/reducer';
import verifyEmailForReset from 'containers/VerifyEmailForReset/reducer';
import activateError from 'containers/UserActivate/reducer';
import resetNotAllowed from 'containers/ResetPassword/reducer';
import resendEmail from 'containers/ResendActivationEmail/reducer';
import { LOG_OUT } from 'containers/Login/constants';

export default function createReducer() {
  const appReducer = combineReducers({
    registrationErrors,
    login,
    verifyEmailForReset,
    activateError,
    resetNotAllowed,
    resendEmail,
    routing: routerReducer,
    form,
    dashboard,
    category,
  });

  return (state, action) => {
    let newState = state;

    if (action.type === LOG_OUT) {
      newState = undefined;
    }

    return appReducer(newState, action);
  };
}
