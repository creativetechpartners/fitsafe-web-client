import { fork } from 'redux-saga/effects';
import dashboard from 'containers/Dashboard/saga';
import category from 'containers/Category/saga';
import registrationSaga from 'containers/Registration/saga';
import loginSaga from 'containers/Login/saga';
import activationSaga from 'containers/UserActivate/saga';
import submitEmailForResetSaga from 'containers/VerifyEmailForReset/saga';
import resetPasswordSaga from 'containers/ResetPassword/saga';
import resendActivationEmail from 'containers/ResendActivationEmail/saga';
import payment from 'containers/Payment/saga';

export default function* rootSaga() {
  return yield [
    fork(dashboard),
    fork(category),
    fork(registrationSaga),
    fork(loginSaga),
    fork(activationSaga),
    fork(submitEmailForResetSaga),
    fork(resetPasswordSaga),
    fork(resendActivationEmail),
    fork(payment),
  ];
}
