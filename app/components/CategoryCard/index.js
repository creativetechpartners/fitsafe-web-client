import React from 'react';
import { object, func, number, bool } from 'prop-types';
import ScoringCircle from 'components/ScoringCircle';
import './styles.scss';

const CategoryCard = ({ category, answersQuantity, goTo, score, maxScore, showScore }) => (
  <div className="category-card">
    {/* <div className="info">i</div> */}
    <div className="title">{category.name}</div>

    <div className="description">
      {category.description} {showScore ? 'true' : 'false'}
    </div>

    <div className="detailsContainer">
      <div className="questions">
        <div className="questionsTitle">QUESTIONS</div>
        <div className="questionsText">
          <div className="questionsDone">{answersQuantity}</div>
          <div className="questionsTotal">/{category.questionsTotal}</div>
        </div>
      </div>

      <div className="chartContainer">
        {showScore && (
          <ScoringCircle score={score} maxScore={maxScore} answersQuantity={answersQuantity} />
        )}
      </div>
    </div>
    {!!goTo && (
      <button onClick={() => goTo(`category/${category._id}`)} className="goButton">
        GO
      </button>
    )}
  </div>
);

CategoryCard.defaultProps = {
  score: 0,
  showScore: true,
};

CategoryCard.propTypes = {
  category: object.isRequired,
  goTo: func,
  answersQuantity: number.isRequired,
  score: number,
  showScore: bool,
  maxScore: number.isRequired,
};

export default CategoryCard;
