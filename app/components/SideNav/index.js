import React from 'react';
import { array, func, object } from 'prop-types';
import './styles.scss';

const SideNav = ({ onSelect, surveys, activeSurvey: { _id } }) => (
  <div className="side-nav">
    {surveys.map(survey => (
      <div
        onClick={() => onSelect(survey)}
        className="survey-container"
        key={survey._id}
      >
        <div
          className={`category-item ${survey._id === _id ? 'active' : ''}`}
          role="button"
        >
          {survey.name}
        </div>
      </div>
    ))}
  </div>
);

SideNav.propTypes = {
  surveys: array.isRequired,
  onSelect: func.isRequired,
  activeSurvey: object.isRequired,
};

export default SideNav;
