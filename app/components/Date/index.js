import React from 'react';
import moment from 'moment';
import { string } from 'prop-types';
import './styles.scss';


const Date = ({ className }) => (
  <div className={`dateContainer header-card ${className}`}>
    <h3 className="dayOfWeek">{moment().format('dddd')}</h3>
    <h2 className="date">{moment().format('MMMM Do, YYYY')}</h2>
  </div>
);

Date.defaultProps = {
  className: '',
};

Date.propTypes = {
  className: string,
};

export default Date;
