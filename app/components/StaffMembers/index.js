import React from 'react';
import { string } from 'prop-types';

const StaffMembers = ({ className }) => (
  <div className={`header-card ${className}`}>
    <div className="title">STAFF & MEMBERS</div>
    <div className="infoDescription">102 STAFF & PERSONNEL</div>
    <div className="infoDescription">614 MEMBERS</div>
    <div className="editButton">Edit</div>
  </div>
);

StaffMembers.defaultProps = {
  className: '',
};

StaffMembers.propTypes = {
  className: string,
};

export default StaffMembers;
