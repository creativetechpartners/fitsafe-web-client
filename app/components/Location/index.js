import React from 'react';
import { bool, string } from 'prop-types';
import './styles.scss';

const Location = ({ showTitle, className }) => (
  <div className={`header-card ${className}`}>
    {showTitle && <div className="title">LOCATION</div>}

    <div className="horizontal">
      <div className="infoImg" style={{ backgroundImage: 'url(/img/muzhchina-gantelya-myshcy-mayka.jpg)' }} role="img" />
      <div className="info">
        <div className="infoTitle">Results Fitness</div>
        <div className="infoDescription">123 Main Street</div>
        <div className="infoDescription">Anytown, US 12345</div>
        <div className="editButton">Edit</div>
      </div>
    </div>
  </div>
);

Location.defaultProps = {
  showTitle: true,
  className: '',
};

Location.propTypes = {
  showTitle: bool,
  className: string,
};

export default Location;
