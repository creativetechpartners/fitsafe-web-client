import React from 'react';
import { object, func, any } from 'prop-types';
import { Button } from 'react-bootstrap';
import './style.scss';

const MainQuestion = ({ question, onPress, mainAnswered, nextSurvey, clearSurvey }) => {
  const { title } = question;
  return (
    <div className={`${mainAnswered === true ? 'answered-yes' : ''} main-question`}>
      <div className="title">{title}</div>
      <div className="main-question-controls">
        <Button
          bsStyle="info"
          className={`${mainAnswered === false ? 'yes-answer active' : 'yes-answer inactive'} btn-raised`}
          key={'yes'}
          onClick={() => onPress(true)}
        >
          YES
        </Button>
        <Button
          bsStyle="info"
          className={`${mainAnswered === true ? 'no-answer active' : 'no-answer inactive'} btn-raised`}
          key={'no'}
          onClick={() => nextSurvey(false)}
        >
          NO
        </Button>

        <Button
          bsStyle="info"
          className={'btn-raised reset-btn'}
          key={'reset'}
          onClick={clearSurvey}
        >
          CLEAR
        </Button>
      </div>
    </div>
  );
};

MainQuestion.defaultProps = {
  mainAnswered: null,
};

MainQuestion.propTypes = {
  question: object.isRequired,
  onPress: func.isRequired,
  mainAnswered: any,
  nextSurvey: func.isRequired,
  clearSurvey: func.isRequired,
};

export default MainQuestion;
