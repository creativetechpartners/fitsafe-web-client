import React from 'react';
import { number } from 'prop-types';

const getPercent = (fullScoringValue, scoringValue) => {
  return fullScoringValue ? 100 - scoringValue / fullScoringValue * 100 : 0;
};

const getRoughPercent = (...args) => {
  return Math.round(getPercent(...args) / 10) * 10;
};

const getImageUrl = (image, answersQuantity) => {
  if (answersQuantity === 0) {
    return '/img/circles/empty.png';
  }

  return `/img/circles/${image}.png`;
};

const ScoringCircle = ({ score, maxScore, answersQuantity }) =>
  (<div className="chart">
    <img src={getImageUrl(getRoughPercent(maxScore, score), answersQuantity)} alt="chart" />
    <div className="chartPercent">
      {answersQuantity === 0 ? 0 : Math.round(getPercent(maxScore, score))}
    </div>
  </div>);

ScoringCircle.propTypes = {
  score: number.isRequired,
  maxScore: number.isRequired,
  answersQuantity: number.isRequired,
};

export default ScoringCircle;
