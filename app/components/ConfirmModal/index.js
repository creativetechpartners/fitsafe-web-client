import React from 'react';
import { Modal, Button } from 'react-bootstrap';
import { string, func, bool } from 'prop-types';

const ConfirmModal = ({ show, onHide, title, content, onSubmit }) =>
  (<Modal show={show} onHide={e => e && onHide()}>
    <Modal.Header closeButton>
      <Modal.Title>
        {title}
      </Modal.Title>
    </Modal.Header>

    <Modal.Body bsClass="modal-body clearfix">
      {content}

      <Button onClick={onSubmit} bsStyle="info" className="btn-raised pull-right">
        OK
      </Button>
      <Button onClick={onHide} className="btn-raised pull-right">
        Cancel
      </Button>
    </Modal.Body>
  </Modal>);

ConfirmModal.propTypes = {
  show: bool.isRequired,
  onHide: func.isRequired,
  title: string.isRequired,
  content: string.isRequired,
  onSubmit: func.isRequired,
};

export default ConfirmModal;
