import React from 'react';
import { Modal } from 'react-bootstrap';
import { string, func, bool, any } from 'prop-types';

const AlertModal = ({ show, onHide, title, content }) => (
  <Modal show={show} onHide={e => e && onHide()}>
    <Modal.Header>
      <Modal.Title>{title}</Modal.Title>
    </Modal.Header>

    <Modal.Body bsClass="modal-body clearfix">{content}</Modal.Body>
  </Modal>
);

AlertModal.propTypes = {
  show: bool.isRequired,
  onHide: func.isRequired,
  title: string.isRequired,
  content: any.isRequired,
};

export default AlertModal;
