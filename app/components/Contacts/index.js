import React from 'react';
import { string } from 'prop-types';

//   title: {
//     fontSize: 6,
//     color: '#3fa3bf',
//     marginBottom: 4,
//   },
//   infoDescription: {
//     color: 'gray',
//     fontSize: 6,
//   },
//   editButton: {
//     color: '#3fa3bf',
//     fontSize: 6,
//   },

const Contacts = ({ className }) => (
  <div className={`header-card ${className}`}>
    <div className="title">CONTACTS</div>
    <div className="infoDescription"><span style={{ color: '#fff' }}>O</span> 212.123.4567</div>
    <div className="infoDescription"><span style={{ color: '#fff' }}>F</span> 212.456.1234</div>
    <div className="infoDescription"><span style={{ color: '#fff' }}>E</span> info@resultfitness.com</div>
    <div className="editButton">Edit</div>
  </div>
);

Contacts.defaultProps = {
  className: '',
};

Contacts.propTypes = {
  className: string,
};

export default Contacts;
