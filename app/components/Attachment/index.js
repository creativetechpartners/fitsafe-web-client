import React from 'react';
import { array, bool, func } from 'prop-types';
import './styles.scss';

const Attachment = ({ attachments, openUpload, enabled, onDelete }) => (
  <div className="load-attachment">
    {enabled ? (
      <i className="material-icons noselect enabled" onClick={openUpload}>
        camera_alt
      </i>
    ) : (
      <span className="disabled-icon noselect">
        <i className="material-icons noselect disabled">camera_alt</i>
        <i className="material-icons noselect disable-icon">not_interested</i>
      </span>
    )}
    <div className="attachments-count">{attachments.length || null}</div>
    {!!attachments.length && (
      <div className="delete">
        <i className="material-icons noselect" onClick={onDelete}>
          delete_forever
        </i>
      </div>
    )}
  </div>
);

Attachment.defaultProps = {
  enabled: false,
};

Attachment.propTypes = {
  attachments: array.isRequired,
  enabled: bool,
  openUpload: func.isRequired,
  onDelete: func.isRequired,
};

export default Attachment;
