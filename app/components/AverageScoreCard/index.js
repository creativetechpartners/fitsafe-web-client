import React from 'react';
import { number, func, string } from 'prop-types';
import ScoringCircle from '../ScoringCircle';
import './styles.scss';

const AverageScoreCard = ({ score, maxScore, submitScoring, answersQuantity, reportLink }) => (
  <div className="average-score-card">
    <div className="title">Average Score</div>
    <ScoringCircle
      score={Math.round(score)}
      maxScore={maxScore}
      answersQuantity={answersQuantity}
    />
    <button
      className="goButton"
      onClick={reportLink ? () => window.open(reportLink, '_blank') : submitScoring}
    >
      <div className="goButtonText">{reportLink ? 'DOWNLOAD REPORT' : 'SUBMIT REPORT'}</div>
    </button>
  </div>
);

AverageScoreCard.propTypes = {
  score: number.isRequired,
  maxScore: number.isRequired,
  reportLink: string,
  submitScoring: func.isRequired,
  answersQuantity: number.isRequired,
};

export default AverageScoreCard;
