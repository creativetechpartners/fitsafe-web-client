import React, { Component } from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { bindActionCreators } from 'redux';
import { func, string, bool } from 'prop-types';
import { get } from 'lodash';
import { logOut } from 'containers/Login/actions';
import './styles.scss';

const getTitle = (categories, routing) => {
  const { pathname } = routing.locationBeforeTransitions;
  const parts = pathname.split('/');
  const categoryName = parts[parts.length - 1];
  return get(categories, `${categoryName}.name`, null);
};

class NavbarComponent extends Component {
  handleGoTo = () => {
    this.props.goTo('/dashboard');
  };

  render() {
    return (
      <div className="top-navbar">
        <div className="header">
          {this.props.title && <div className="go-back-button" onClick={this.handleGoTo}>
            {'< '} <span className="go-back-button-text">Dashboard</span>
          </div>}
          <h1 className="brand-logo">
            <span role="link" onClick={this.handleGoTo}>
              {this.props.title || 'FitSafe'}
            </span>
          </h1>
          {this.props.loggedIn && <div className="log-out-btn" onClick={() => this.props.goOut()}>Log out</div>}
        </div>
        <div className="progressBarContainer">
          <div className="progressBar" />
        </div>
      </div>
    );
  }
}

NavbarComponent.defaultProps = {
  title: null,
};

NavbarComponent.propTypes = {
  title: string,
  goTo: func.isRequired,
  goOut: func.isRequired,
  loggedIn: bool.isRequired,
};

export default connect(
  ({ routing, dashboard: { categories }, login }) =>
     ({
       title: getTitle(categories, routing),
       loggedIn: !!login.token,
     }),
  dispatch => bindActionCreators({
    goTo: push,
    goOut: logOut,
  }, dispatch),
)(NavbarComponent);
