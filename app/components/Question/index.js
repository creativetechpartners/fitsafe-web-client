import YesNo from './YesNo';
import Radio from './Radio';
import Checkbox from './Checkbox';

const types = {
  'yes-no': YesNo,
  radio: Radio,
  checkbox: Checkbox,
};

const Question = props => types[props.question.type](props);

export default Question;
