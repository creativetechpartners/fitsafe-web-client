import React from 'react';
import { object, func, any, array } from 'prop-types';
import { Panel } from 'react-bootstrap';
import Attachment from 'components/Attachment';
import './styles.scss';

const Question = ({ question, pressAnswer, answer, openUpload, attachments, onDelete }) => (
  <Panel key={question._id} className="radio-question">
    <div className="question-title">
      <span>{question.title}</span>
      {!question.options.attachmentsDisabled && (
        <Attachment
          openUpload={() => openUpload(question)}
          attachments={attachments}
          enabled={!!answer}
          onDelete={() => onDelete(question._id)}
        />
      )}
    </div>
    <div>
      {question.options.variants.map((variant, i) => (
        <div className="variant" key={i} onClick={() => pressAnswer({ answer: variant })}>
          <i className="material-icons noselect">
            {`radio_button_${answer === variant ? 'checked' : 'unchecked'}`}
          </i>
          <span>{variant}</span>
        </div>
      ))}
    </div>
  </Panel>
);

Question.defaultProps = {
  answer: null,
};

Question.propTypes = {
  question: object.isRequired,
  pressAnswer: func.isRequired,
  openUpload: func.isRequired,
  attachments: array.isRequired,
  answer: any,
  onDelete: func.isRequired,
};

export default Question;
