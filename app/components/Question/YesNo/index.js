import React from 'react';
import { object, func, any, array } from 'prop-types';
import { Panel } from 'react-bootstrap';
import Attachment from 'components/Attachment';
import './styles.scss';

const YesNoQuestion = ({ question, pressAnswer, answer, openUpload, attachments, onDelete }) => (
  <Panel key={question._id} className="yes-no-question">
    <div className="title">{question.title}</div>
    <div className="actions">
      <i
        onClick={() => pressAnswer({ answer: true })}
        className={`${answer === true ? 'active answer-action' : 'answer-action'} material-icons`}
      >
        done
      </i>
      <i
        onClick={() => {
          pressAnswer({ answer: false });
          if (question.options.disabledCameraOnNo && attachments.length) onDelete(question._id);
        }}
        className={`${answer === false ? 'active answer-action' : 'answer-action'} material-icons`}
      >
        close
      </i>
      {!question.options.attachmentsDisabled && [
        <div className="vdivider" key="divider" />,
        <Attachment
          key="attachment"
          openUpload={() => openUpload(question)}
          attachments={attachments}
          enabled={question.options.disabledCameraOnNo ? answer === true : answer !== undefined}
          onDelete={() => onDelete(question._id)}
        />,
      ]}
    </div>
  </Panel>
);

YesNoQuestion.defaultProps = {
  answer: null,
};

YesNoQuestion.propTypes = {
  question: object.isRequired,
  pressAnswer: func.isRequired,
  answer: any,
  openUpload: func.isRequired,
  attachments: array.isRequired,
  onDelete: func.isRequired,
};

export default YesNoQuestion;
