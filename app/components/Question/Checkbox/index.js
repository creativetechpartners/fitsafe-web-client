import React from 'react';
import { object, func, any, array } from 'prop-types';
import { Panel } from 'react-bootstrap';
import Attachment from 'components/Attachment';
import './styles.scss';

const Checkbox = ({ question, pressAnswer, answer = [], openUpload, attachments, onDelete }) => (
  <Panel key={question._id} className="checkbox-question">
    <div className="question-title">
      <span>{question.title}</span>
      {!question.options.attachmentsDisabled && (
        <Attachment
          openUpload={() => openUpload(question)}
          attachments={attachments}
          enabled={answer.length > 0}
          onDelete={() => onDelete(question._id)}
        />
      )}
    </div>

    <div>
      {question.options.variants.map((variant, i) => (
        <div
          className="variant"
          key={i}
          // eslint-disable-next-line no-bitwise
          onClick={() =>
            pressAnswer({
              answer: variant,
              wasChecked: !!~answer.indexOf(variant),
              variants: answer,
            })}
        >
          <i className="material-icons noselect">
            {// eslint-disable-next-line no-bitwise
            `check_box${~answer.indexOf(variant) ? '' : '_outline_blank'}`}
          </i>
          <span>{variant}</span>
        </div>
      ))}
    </div>
  </Panel>
);

Checkbox.defaultProps = {
  answer: null,
};

Checkbox.propTypes = {
  question: object.isRequired,
  pressAnswer: func.isRequired,
  openUpload: func.isRequired,
  attachments: array.isRequired,
  answer: any,
  onDelete: func.isRequired,
};

export default Checkbox;
