import React from 'react';
import Location from 'components/Location';
import Date from 'components/Date';
import './styles.scss';

const CategoryHeader = () => (
  <div className="category-header">
    <Location showTitle={false} className="dashboardLocation" />
    <div className="vdivider" />
    <Date />
    <div className="vdivider" />
    <div className="header-card">
      AT A GLANCE SCOPE
    </div>
  </div>
);

export default CategoryHeader;
