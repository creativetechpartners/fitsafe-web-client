import { combineReducers } from 'redux';
import _ from 'lodash';
import { SCORING_LOADED, CATEGORIES_SURVEYS_LOADED } from 'containers/Dashboard/constants';
import combineActions from '../../utils/combineActions';
import {
  CATEGORY_SURVEYS_LOADED,
  QUESTION_ANSWERED,
  MAIN_QUESTION_ANSWERED,
  FILES_ADDED,
  DELETE_FILES,
  DELETE_QUESTIONS_SCORING,
  CLEAR_SURVEY,
  RESET_ALL,
} from './constants';

const deleteKey = (obj, key) =>
  Object.keys(obj).reduce((a, k) => (k === key ? a : { ...a, [k]: obj[k] }), {});

const deleteKeys = (obj, arrayOfKeys) =>
  Object.keys(obj).reduce((a, k) => (~arrayOfKeys.indexOf(k) ? a : { ...a, [k]: obj[k] }), {});

const getScoreValue = (question, answer) => {
  const {
    type,
    options: { variantsScores = [], variants = [] },
  } = question;
  if (type === 'yes-no') {
    return answer ? variantsScores[0] : variantsScores[1];
  }
  return variantsScores[variants.findIndex(e => e === answer)];
};

const toggleOption = (answers, newAnswer) => {
  if (answers.indexOf(newAnswer) === -1) {
    return [...answers, newAnswer];
  }

  const turnedOff = answers.filter(el => el !== newAnswer);
  if (!turnedOff.length) {
    return null;
  }
  return turnedOff;
};

const getCategoryScores = (state, { categoryId, question, answer, wasChecked, variants }) => {
  const categoryScores = state[categoryId] || {};
  const scoreValue = Math.abs(getScoreValue(question, answer)) || 0;
  const prevValue = _.get(state, `[${categoryId}][${question._id}]`, 0);

  if (question.type === 'checkbox') {
    const clearSurvey = variants.length === 1 && variants[0] === answer;

    return {
      ...state,
      [categoryId]: !clearSurvey
        ? {
            ...categoryScores,
            [question._id]: wasChecked ? prevValue - scoreValue : prevValue + scoreValue,
          }
        : deleteKey(categoryScores, question._id),
    };
  }

  return {
    ...state,
    [categoryId]: {
      ...categoryScores,
      [question._id]: scoreValue,
    },
  };
};

const getCategoryScoresForMainQuestion = (state, { categoryId, survey, answer }) => {
  const questionsId = Object.keys(survey.questions);
  const categoryScores = state[categoryId] || {};

  const questionsWithMaxScores = Object.values(survey.questions).reduce((acc, question) => {
    if (question.type !== 'checkbox') {
      return { ...acc, [question._id]: Math.max(...question.options.variantsScores) };
    }
    return { ...acc, [question._id]: question.options.variantsScores.reduce((a, c) => a + c, 0) };
  }, {});

  if (answer === false && !survey.significant) {
    return {
      ...state,
      [categoryId]: deleteKeys(categoryScores, questionsId),
    };
  }

  if (answer === false && survey.significant) {
    return {
      ...state,
      [categoryId]: {
        ...categoryScores,
        ...questionsWithMaxScores,
      },
    };
  }
  if (answer === true && survey.significant) {
    return {
      ...state,
      [categoryId]: deleteKeys(categoryScores, Object.keys(survey.questions)),
    };
  }

  return state;
};

const clearSurveyScore = (state, { categoryId, survey }) => {
  const questionsId = Object.keys(survey.questions);
  const categoryScores = state[categoryId] || {};

  return {
    ...state,
    [categoryId]: deleteKeys(categoryScores, questionsId),
  };
};

const addAttachments = (state, { questionId, attachments }) => {
  return {
    ...state,
    [questionId]: [...(state[questionId] || []), ...attachments],
  };
};

const removeAttachments = (state, { questionId }) => {
  return deleteKey(state, questionId);
};

const questionAnswered = (state, { categoryId, surveyId, question, answer }) => {
  const category = state[categoryId] || {};
  const survey = category[surveyId] || {};

  if (question.type === 'checkbox') {
    const toggledArray = toggleOption(
      _.get(state, `[${categoryId}][${surveyId}].answers[${question._id}]`, []),
      answer,
    );

    const deleteAnswers = Object.keys(survey.answers || {}).reduce((acc, key) => {
      if (key === question._id) return acc;
      acc[key] = survey.answers[key];
      return acc;
    }, {});

    return {
      ...state,
      [categoryId]: {
        ...category,
        [surveyId]: {
          ...survey,
          answers: toggledArray
            ? {
                ...survey.answers,
                [question._id]: toggledArray,
              }
            : deleteAnswers,
        },
      },
    };
  }

  return {
    ...state,
    [categoryId]: {
      ...category,
      [surveyId]: {
        ...survey,
        answers: {
          ...survey.answers,
          [question._id]: answer,
        },
      },
    },
  };
};

const removeScoring = (state, { categoryId, surveyId, questionsIds }) => {
  const category = state[categoryId] || {};
  const survey = category[surveyId] || {};

  return {
    ...state,
    [categoryId]: {
      ...category,
      [surveyId]: {
        ...survey,
        answers: deleteKeys(survey.answers || {}, questionsIds),
      },
    },
  };
};

const clearSurveyAttachments = (state, { answer, survey }) => {
  const questionsId = Object.keys(survey.questions);

  if (typeof answer !== 'undefined') {
    if (answer === false) {
      return deleteKeys(state, questionsId);
    }

    return state;
  }

  return deleteKeys(state, questionsId);
};

const mainQuestionAnswered = (state, { categoryId, surveyId, answer, survey }) => {
  const category = state[categoryId] || {};

  if (answer === false) {
    if (!survey.significant) {
      return {
        ...state,
        [categoryId]: {
          ...category,
          [surveyId]: {
            ...deleteKey(survey, 'answers'),
            mainQuestion: answer,
          },
        },
      };
    }

    const answersWithMaxScore = Object.values(survey.questions).reduce((acc, question) => {
      let answer;

      const { variantsScores, variants } = question.options;

      if (question.type === 'yes-no') {
        const maxScore = Math.max(...variantsScores);
        const index = question.options.variantsScores.indexOf(maxScore);
        if (~index) {
          answer = variants[index] === 'yes';
        }
      }

      if (question.type === 'radio') {
        const maxScore = Math.max(...variantsScores);
        const index = question.options.variantsScores.indexOf(maxScore);
        if (~index) {
          answer = variants[index];
        }
      }

      if (question.type === 'checkbox') {
        const answers = [];
        variantsScores.forEach((score, i) => {
          if (score) {
            answers.push(variants[i]);
          }
        });

        answer = answers;
      }

      if (typeof answer === 'undefined') {
        return acc;
      }

      return { ...acc, [question._id]: answer };
    }, {});

    return {
      ...state,
      [categoryId]: {
        ...category,
        [surveyId]: {
          answers: answersWithMaxScore,
          mainQuestion: answer,
        },
      },
    };
  }

  return {
    ...state,
    [categoryId]: {
      ...category,
      [surveyId]: {
        ...survey,
        mainQuestion: answer,
      },
    },
  };
};

const clearSurvey = (state, { categoryId, survey }) => {
  const category = state[categoryId] || {};

  return {
    ...state,
    [categoryId]: deleteKey(category, survey._id),
  };
};

const scoring = combineActions(
  {
    [QUESTION_ANSWERED]: questionAnswered,
    [MAIN_QUESTION_ANSWERED]: mainQuestionAnswered,
    [CLEAR_SURVEY]: clearSurvey,
    [RESET_ALL]: () => ({}),
    [SCORING_LOADED]: (state, action) => action.scoring.scoring || state,
    [DELETE_QUESTIONS_SCORING]: removeScoring,
  },
  {},
);

const surveys = combineActions(
  {
    [CATEGORY_SURVEYS_LOADED]: (state, action) => ({ ...state, ...action.surveys }),
    [CATEGORIES_SURVEYS_LOADED]: (state, action) => ({ ...state, ...action.categoriesSurveys }),
  },
  {},
);

const categoriesScores = combineActions(
  {
    [QUESTION_ANSWERED]: getCategoryScores,
    [SCORING_LOADED]: (state, action) => action.scoring.categoriesScores || state,
    [MAIN_QUESTION_ANSWERED]: getCategoryScoresForMainQuestion,
    [CLEAR_SURVEY]: clearSurveyScore,
    [RESET_ALL]: () => ({}),
  },
  {},
);

const attachments = combineActions(
  {
    [FILES_ADDED]: addAttachments,
    [SCORING_LOADED]: (state, action) => action.scoring.attachments || state,
    [DELETE_FILES]: removeAttachments,
    [MAIN_QUESTION_ANSWERED]: clearSurveyAttachments,
    [CLEAR_SURVEY]: clearSurveyAttachments,
    [RESET_ALL]: () => ({}),
  },
  {},
);

export default combineReducers({
  surveys,
  scoring,
  categoriesScores,
  attachments,
});
