import {
  LOAD_CATEGORY_SURVEYS,
  CATEGORY_SURVEYS_LOADED,
  ANSWER_MAIN_QUESTION,
  CLEAR_SURVEY,
  ANSWER_QUESTION,
  UPLOAD_FILES,
  FILES_ADDED,
  DELETE_FILES,
  DELETE_QUESTIONS_SCORING,
  RESET_ALL,
} from './constants';

export const loadCategorySurveys = category => ({
  type: LOAD_CATEGORY_SURVEYS,
  category,
});

export const categorySurveysLoaded = surveys => ({
  type: CATEGORY_SURVEYS_LOADED,
  surveys,
});

export const answerQuestion = answerOptions => ({
  type: ANSWER_QUESTION,
  ...answerOptions,
});

export const answerMainQuestion = answerOptions => ({
  type: ANSWER_MAIN_QUESTION,
  ...answerOptions,
});

export const clearSurvey = answerOptions => ({
  type: CLEAR_SURVEY,
  ...answerOptions,
});

export const uploadFiles = ({ filesList, questionId, categoryId, surveyId }) => ({
  type: UPLOAD_FILES,
  filesList,
  questionId,
  surveyId,
  categoryId,
});

export const deleteFiles = questionId => ({
  type: DELETE_FILES,
  questionId,
});

export const filesUploaded = data => ({
  type: FILES_ADDED,
  ...data,
});

export const deleteQuestions = args => ({
  type: DELETE_QUESTIONS_SCORING,
  ...args,
});

export const resetAll = () => ({
  type: RESET_ALL,
});
