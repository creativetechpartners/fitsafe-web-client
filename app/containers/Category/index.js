import React, { Component } from 'react';
import { object, func } from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
// import _ from 'lodash';
// import CategoryHeader from 'components/CategoryHeader';
import SideNav from 'components/SideNav';
import Questions from 'containers/Questions';
import ConfirmModal from 'components/ConfirmModal';
import {
  loadCategorySurveys,
  answerMainQuestion,
  clearSurvey,
  answerQuestion,
  uploadFiles,
  deleteFiles,
  deleteQuestions,
} from './actions';
import './style.scss';

class Category extends Component {
  state = {
    surveyId: Object.keys(this.surveys)[0],
    currentUploadQuestion: null,
    questionIdToDeleteAttachment: null,
    showConfirm: false,
  };

  componentDidMount() {
    const { category } = this.props.params;
    this.props.loadCategorySurveys(category);
  }

  componentWillReceiveProps({ surveys }) {
    const { category } = this.props.params;
    const surveysIds = Object.values(surveys)
      .filter(survey => survey.category === category)
      .map(s => s._id);

    if (surveysIds.length) {
      this.setState({ surveyId: surveysIds[0] });
    }
  }

  openUpload = (question) => {
    this.setState({ currentUploadQuestion: question }, () => this.file.click());
  };

  isImage = (file) => {
    const fileExtension = file.split('.').pop();
    const isImage = ['bmp', 'jpg', 'jpeg', 'png', 'gif'].includes(fileExtension.toLowerCase());
    if (!isImage) {
      console.warn('Not an image', `[${fileExtension}]`);
    }
    return isImage;
  };

  fileSelected = () => {
    if (this.isImage(this.file.value)) {
      const { category: categoryId } = this.props.params;
      this.props.uploadFiles({
        filesList: this.file.files,
        questionId: this.state.currentUploadQuestion._id,
        categoryId,
        surveyId: this.state.surveyId,
      });
      this.file.value = '';
      this.setState({ currentUploadQuestion: null });
    }
  };

  handleDelete = () => {
    const { questionIdToDeleteAttachment } = this.state;
    this.props.deleteFiles(questionIdToDeleteAttachment);
    this.setState({ questionIdToDeleteAttachment: null, showConfirm: false });
  };

  askForDelete = (questionId) => {
    this.setState({
      showConfirm: true,
      questionIdToDeleteAttachment: questionId,
    });
  };

  hideConfirm = () => {
    this.setState({ showConfirm: false });
  };

  get surveys() {
    const { category } = this.props.params;
    return Object.values(this.props.surveys).reduce((acc, survey) => {
      if (survey.category !== category) return acc;
      return { ...acc, [survey._id]: survey };
    }, {});
  }

  nextSurvey = (...args) => {
    const ids = Object.keys(this.surveys);
    const next = ids[ids.indexOf(this.state.surveyId) + 1];
    this.props.answerMainQuestion(...args);
    if (next) {
      this.setState({ surveyId: next });
    }
  };

  renderQuestionsList = (categoryId, activeSurvey) => {
    const surveysId = Object.keys(this.surveys);
    const activeSurveyIndex = surveysId.indexOf(activeSurvey._id);
    const prevId = surveysId[activeSurveyIndex - 1];
    const nextId = surveysId[activeSurveyIndex + 1];

    return (
      <Questions
        answerMainQuestion={this.props.answerMainQuestion}
        clearSurvey={this.props.clearSurvey}
        answerQuestion={this.props.answerQuestion}
        survey={activeSurvey}
        categoryId={categoryId}
        openUpload={this.openUpload}
        onDelete={this.askForDelete}
        prevId={prevId}
        nextId={nextId}
        onSelect={surveyId => this.setState({ surveyId })}
        nextSurvey={this.nextSurvey}
        deleteQuestions={this.props.deleteQuestions}
      />
    );
  };

  render() {
    const { surveys } = this.props;
    const { surveyId } = this.state;
    const { category } = this.props.params;
    const activeSurvey = surveys[surveyId];

    return (
      <div className="category-container">
        {/* <CategoryHeader /> */}
        <div className="content-container">
          <ConfirmModal
            show={this.state.showConfirm}
            onSubmit={this.handleDelete}
            title="Attachment delete"
            content="Are you sure you want to delete attachment?"
            onHide={this.hideConfirm}
          />

          <SideNav
            surveys={Object.values(surveys).filter(survey => survey.category === category)}
            activeSurvey={activeSurvey || {}}
            onSelect={survey => this.setState({ surveyId: survey._id })}
          />

          {activeSurvey && this.renderQuestionsList(category, activeSurvey)}

          <input
            accept=".jpg, .png, .jpeg, .gif, .bmp, .tif, .tiff|images/*"
            type="file"
            onChange={this.fileSelected}
            className="hidden"
            ref={(ref) => {
              this.file = ref;
            }}
          />
        </div>
      </div>
    );
  }
}

Category.propTypes = {
  loadCategorySurveys: func.isRequired,
  surveys: object.isRequired,
  answerMainQuestion: func.isRequired,
  answerQuestion: func.isRequired,
  params: object.isRequired,
  uploadFiles: func.isRequired,
  deleteFiles: func.isRequired,
  deleteQuestions: func.isRequired,
  clearSurvey: func.isRequired,
};

export default connect(
  ({ category: { surveys } }) => ({ surveys }),
  dispatch =>
    bindActionCreators(
      {
        loadCategorySurveys,
        answerMainQuestion,
        answerQuestion,
        goBack: push,
        uploadFiles,
        deleteFiles,
        deleteQuestions,
        clearSurvey,
      },
      dispatch,
    ),
)(Category);
