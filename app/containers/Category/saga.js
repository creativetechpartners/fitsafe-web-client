import { takeEvery, put, select } from 'redux-saga/effects';
import api from '../../utils/api';
import {
  LOAD_CATEGORY_SURVEYS,
  UPLOAD_FILES,
  ANSWER_QUESTION,
  ANSWER_MAIN_QUESTION,
  QUESTION_ANSWERED,
  MAIN_QUESTION_ANSWERED,
  DELETE_FILES,
  DELETE_QUESTIONS_SCORING,
  CLEAR_SURVEY,
  RESET_ALL,
} from './constants';
import { categorySurveysLoaded, filesUploaded, deleteQuestions } from './actions';

function* loadCategorySurveys({ category }) {
  try {
    const { surveys } = yield api.get('survey', {
      query: {
        category,
      },
    });
    yield put(categorySurveysLoaded(surveys));
  } catch (err) {
    console.log(err); // eslint-disable-line no-console
  }
}

function* syncScoring() {
  const { scoring, categoriesScores, attachments } = yield select(({ category }) => category);
  yield api.post('scoring', {
    body: {
      scoring: {
        scoring,
        categoriesScores,
        attachments,
      },
    },
  });
}

function* uploadFiles({ filesList, questionId, categoryId, surveyId }) {
  try {
    const body = new FormData();
    for (let i = 0; i < filesList.length; i += 1) {
      body.append('files', filesList.item(i));
    }
    const { attachments } = yield api.post(`attachment/${questionId}`, {
      body,
    });
    yield put(filesUploaded({ attachments, categoryId, surveyId, questionId }));
    yield syncScoring();
  } catch (err) {
    console.trace(err); // eslint-disable-line no-console
  }
}

function* clearScoringForHidden({ categoryId, surveyId }) {
  const state = yield select(store => store.category);
  const category = state.scoring[categoryId] || {};
  const survey = category[surveyId] || {};
  const { questions } = state.surveys[surveyId];
  const idsToDelete = [];

  Object.values(questions).forEach(question => {
    if (question.show) {
      const targetQuestion = Object.values(questions).find(q => q._id === question.show.questionId);

      if (!(targetQuestion && survey.answers[targetQuestion._id] === question.show.condition)) {
        idsToDelete.push(question._id);
      }
    }
  });

  yield put(
    deleteQuestions({
      questionsIds: idsToDelete,
      categoryId,
      surveyId,
    }),
  );

  yield syncScoring();
}

function* syncQuestion(action) {
  yield put({ ...action, type: QUESTION_ANSWERED });
  // yield syncScoring();
}

function* syncMainQuestion(action) {
  yield put({ ...action, type: MAIN_QUESTION_ANSWERED });
  yield syncScoring();
}

function* deleteFiles({ questionId }) {
  try {
    yield api.delete(`attachment/${questionId}`, {
      body: {},
    });
  } catch (err) {
    console.log(err);
  }
  yield syncScoring();
}

// function* deleteCategoryFiles({ answer, survey }) {
//   if (answer === false) {
//     try {
//       yield Promise.all(
//         Object.keys(survey.questions).map((questionId) => {
//           return api.delete(`attachment/${questionId}`, {
//             body: {},
//           });
//         }),
//       );
//     } catch (err) {
//       console.log(err);
//     }
//   }
//   yield syncScoring();
// }

export default function* categorySaga() {
  yield* [
    takeEvery(LOAD_CATEGORY_SURVEYS, loadCategorySurveys),
    takeEvery(UPLOAD_FILES, uploadFiles),
    takeEvery(ANSWER_QUESTION, syncQuestion),
    takeEvery(ANSWER_QUESTION, clearScoringForHidden),
    takeEvery(ANSWER_MAIN_QUESTION, syncMainQuestion),
    takeEvery(DELETE_FILES, deleteFiles),
    takeEvery(DELETE_QUESTIONS_SCORING, syncScoring),
    takeEvery(CLEAR_SURVEY, syncScoring),
    takeEvery(RESET_ALL, syncScoring),
    // takeEvery(MAIN_QUESTION_ANSWERED, deleteCategoryFiles),
  ];
}
