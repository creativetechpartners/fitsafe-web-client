import React from 'react';
import { bool } from 'prop-types';
import { connect } from 'react-redux';
import './styles.scss';

const ThankYou = ({ showError }) => {
  const WaitMessage = () => <h3>Please wait until we create your report...</h3>;

  const ErrorMessage = () => (
    <div>
      <h2>Cannot generate report. Please try again later.</h2>
      <a href="/dashboard">RETURN TO MAIN PAGE</a>
    </div>
  );

  return <div className="thank-you-page">{!showError ? <WaitMessage /> : <ErrorMessage />}</div>;
};

ThankYou.propTypes = {
  showError: bool.isRequired,
};

export default connect(({ dashboard: { reportGenerationError } }) => ({
  showError: reportGenerationError,
}))(ThankYou);
