import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import StripeCheckout from 'react-stripe-checkout';
import { object } from 'prop-types';
import { charge } from './actions';
import './style.scss';

class Payment extends Component {
  state = {
    showSpinner: false,
  };

  showSpinner = () => this.setState({ showSpinner: true });

  render() {
    const { user, charge } = this.props;
    const { showSpinner } = this.state;

    if (showSpinner) {
      return (
        <div className="shadow-payment">
          <img src="img/stripe_spinner.png" alt="spinner" />
        </div>
      );
    }

    return (
      <div className="shadow-payment">
        <h3 className="description-payment">
          Please use the button below to securely enter your billing information for your FitSafe
          subscription, which is $6.99 per month for a period of 12 months.
          Your subscription auto-renews for subsequent contiguous 12 month periods unless cancelled 30 days before the renewal date.
        </h3>
        <h4 className="description-payment">
          By clicking "Pay With Card," you affirm that you have read and agree to the terms of
          purchase at <a href="https://fitsafe.net/terms">Terms of Service</a>.
        </h4>
        <StripeCheckout
          token={(...args) => {
            this.showSpinner();
            charge(...args);
          }}
          stripeKey="pk_test_j9IRuOJEzHkjpPoNR3A2htWW"
          name="$6.99 per month"
          panelLabel="Pay"
          amount={699}
          currency="USD"
          email={user.email}
        />
        <p className="error_pay_message">error</p>
      </div>
    );
  }
}

Payment.propTypes = {
  user: object.isRequired,
};

export default connect(
  ({ login }) => ({ user: login.user }),
  dispatch => bindActionCreators({ charge }, dispatch),
)(Payment);
