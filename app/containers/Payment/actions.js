import { CHARGE } from './constants';

export const charge = token => ({
  type: CHARGE,
  token,
});
