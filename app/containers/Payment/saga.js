import { takeEvery, put } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import api from 'utils/api';
import { putUser } from 'containers/Login/actions';
import { CHARGE } from './constants';


function* chargeSaga({ token: stripeData }) {
  try {
    const user = yield api.post('charge', {
      body: {
        stripeEmail: stripeData.email,
        stripeToken: stripeData.id,
      },
    });

    if (!user) throw new Error('Can\'t subscribe');

    yield put(putUser(user));

    yield put(push('/dashboard'));
  } catch (err) {
    console.error(err.message);
    yield put(push('/payment-error'));
  }
}

export default function* paymentSaga() {
  yield* [takeEvery(CHARGE, chargeSaga)];
}
