import React from 'react';
import './style.scss';

const AfterRegistration = () => (
  <div>
    <h1 className="after-register-text">Verification mail has been sent to your e-mail.</h1>
  </div>
);

export default AfterRegistration;
