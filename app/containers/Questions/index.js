import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button } from 'react-bootstrap';
import { object, func, string } from 'prop-types';
import Question from '../../components/Question';
import MainQuestion from '../../components/MainQuestion';
import './style.scss';

class Questions extends Component {
  renderQuestionItem = (question) => {
    const {
      survey,
      answerQuestion,
      categoryId,
      scoringSurvey,
      openUpload,
      attachments,
      onDelete,
      clearSurvey,
    } = this.props;
    const { answers = {} } = scoringSurvey;
    const { _id: questionId } = question;

    if (question.show) {
      const targetQuestion = Object.values(survey.questions).find(
        q => q._id === question.show.questionId,
      );

      if (!(targetQuestion && answers[targetQuestion._id] === question.show.condition)) {
        return null;
      }
    }

    return (
      <Question
        key={question._id}
        question={question}
        answer={answers[question._id]}
        openUpload={openUpload}
        onDelete={onDelete}
        attachments={attachments[questionId] || []}
        pressAnswer={opts =>
          answerQuestion({
            categoryId,
            surveyId: survey._id,
            question,
            ...opts,
          })}
      />
    );
  };

  render() {
    const {
      survey,
      answerMainQuestion,
      categoryId,
      scoringSurvey,
      prevId,
      nextId,
      onSelect,
      nextSurvey,
      clearSurvey,
    } = this.props;
    const { mainQuestion } = scoringSurvey;

    return (
      <div className={`${mainQuestion === true ? 'answered-yes' : ''} questions-container`}>
        <MainQuestion
          mainAnswered={mainQuestion}
          question={survey.surveyQuestion || {}}
          clearSurvey={() => clearSurvey({ categoryId, survey })}
          onPress={answer =>
            answerMainQuestion({ categoryId, surveyId: survey._id, answer, survey })}
          nextSurvey={answer => nextSurvey({ categoryId, surveyId: survey._id, answer, survey })}
        />
        <div className="questions-list">
          {mainQuestion && Object.values(survey.questions).map(this.renderQuestionItem)}
        </div>
        {mainQuestion === true && (
          <div className="questions-controls">
            <Button bsStyle="info" className={!prevId ? 'hidden-btn' : ''} disabled={!prevId} onClick={() => onSelect(prevId)}>
              BACK
            </Button>
            <Button bsStyle="info" className={!nextId ? 'hidden-btn' : ''} disabled={!nextId} onClick={() => onSelect(nextId)}>
              NEXT
            </Button>
          </div>
        )}
      </div>
    );
  }
}

Questions.defaultProps = {
  prevId: null,
  nextId: null,
};

Questions.propTypes = {
  survey: object.isRequired,
  answerQuestion: func.isRequired,
  answerMainQuestion: func.isRequired,
  categoryId: string.isRequired,
  scoringSurvey: object.isRequired,
  openUpload: func.isRequired,
  attachments: object.isRequired,
  prevId: string,
  nextId: string,
  onSelect: func.isRequired,
  onDelete: func.isRequired,
  deleteQuestions: func.isRequired,
  clearSurvey: func.isRequired,
};

export default connect(({ category: { scoring, attachments } }, ownProps) => {
  const { categoryId, survey } = ownProps;
  const categoryScoring = scoring[categoryId] || {};
  const scoringSurvey = categoryScoring[survey._id] || {};
  return {
    scoringSurvey,
    attachments,
  };
})(Questions);
