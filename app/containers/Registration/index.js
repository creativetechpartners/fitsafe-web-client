import React from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import { bindActionCreators } from 'redux';
import { push } from 'react-router-redux';
import { func, shape, string } from 'prop-types';
import Input from 'components/Input';
import { registrationUser } from './actions';
import './style.scss';


const RegistrationForm = (props) => {
  const { handleSubmit } = props;
  const {
    policyNumber,
    insuranceCarrier,
    businessAddress,
    businessName,
    firstName,
    lastName,
    email,
    password,
  } = props.errors;

  return (
    <div className="registration">
      <img className="registration__logo" src="/img/loginLogo.png" alt="fitsafe logo" />
      <form className="registration__form" onSubmit={handleSubmit} autoComplete="off">
        <h1>Company information</h1>
        <Field
          name="businessName"
          placeholder="Legal name of business"
          component={Input}
          type="text"
          error={businessName}
        />
        <Field
          name="businessAddress"
          placeholder="Address of business"
          component={Input}
          type="text"
          error={businessAddress}
        />
        <Field
          name="insuranceCarrier"
          placeholder="Insurance Carrier"
          component={Input}
          type="text"
          error={insuranceCarrier}
        />
        <Field
          name="policyNumber"
          placeholder="Policy Number"
          component={Input}
          type="text"
          error={policyNumber}
        />

        <h1>Your information</h1>
        <Field
          name="firstName"
          placeholder="First Name"
          component={Input}
          type="text"
          error={firstName}
        />
        <Field
          name="lastName"
          placeholder="Last Name"
          component={Input}
          type="text"
          error={lastName}
        />
        <Field
          name="email"
          placeholder="Email"
          component={Input}
          type="text"
          error={email}
        />
        <Field
          name="password"
          placeholder="Password"
          component={Input}
          type="password"
          error={password}
        />
        <div className="registration__error">{props.err}</div>
        <button type="submit" className="registration__btn btn btn-raised btn-info">Sign up</button>
      </form>
      <div className="registration__actions">
        <a onClick={() => props.push('/login')}>Already have an account?</a>
      </div>
    </div>
  );
};

RegistrationForm.defaultProps = {
  err: undefined,
};

RegistrationForm.propTypes = {
  handleSubmit: func.isRequired,
  errors: shape({
    firstName: string,
    lastName: string,
    email: string,
    password: string,

    businessName: string,
    businessAddress: string,
    insuranceCarrier: string,
    policyNumber: string,
  }).isRequired,
  err: string,
  push: func.isRequired,
};

const wrappedInForm = reduxForm({
  form: 'registration',
  onSubmit: (data, dispatch) => dispatch(registrationUser(data)),
})(RegistrationForm);

export default connect(
  ({ registrationErrors }) => ({ errors: registrationErrors }),
  dispatch => bindActionCreators({
    push,
  }, dispatch),
)(wrappedInForm);

