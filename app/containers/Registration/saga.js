import { takeEvery, put } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import api from '../../utils/api';
import { USER_REGISTER } from './constants';
import { registrationUserFail } from './actions';

function* registration({ data }) {
  try {
    yield api.post('user', { body: data });
    yield put(push('/after_registration'));
  } catch (err) {
    yield put(registrationUserFail(err.error));
  }
}

export default function* registrationSaga() {
  yield* [takeEvery(USER_REGISTER, registration)];
}
