import { USER_REGISTER_FAIL, USER_REGISTER } from './constants';

export const registrationUser = data => ({
  type: USER_REGISTER,
  data,
});

export const registrationUserFail = errors => ({
  type: USER_REGISTER_FAIL,
  errors,
});
