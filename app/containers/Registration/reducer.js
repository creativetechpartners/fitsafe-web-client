import { USER_REGISTER_FAIL } from './constants';
import combineActions from '../../utils/combineActions';

export default combineActions({
  [USER_REGISTER_FAIL]: (state, { errors }) => errors,
}, {});
