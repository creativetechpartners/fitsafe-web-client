import React, { Component } from 'react';
import { push } from 'react-router-redux';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import './style.scss';

class PaymentError extends Component {
  render() {
    return (
      <div className="payment-error-container">
        <h1 className="after-verify-msg">Payment Error</h1>
        <a onClick={() => this.props.push('/')}>Back to the payment page</a>
      </div>
    );
  }
}

export default connect(null, dispatch => bindActionCreators({ push }, dispatch))(PaymentError);
