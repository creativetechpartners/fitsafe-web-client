import React from 'react';
import { node } from 'prop-types';
import NavBar from 'components/NavBar';
import './styles.scss';

const Layout = props => (
  <div className="layout">
    <div className="backgroundCover" style={{ backgroundImage: 'url(/img/background_main.jpg)' }} />
    <NavBar />
    {React.Children.toArray(props.children)}
  </div>
);

Layout.propTypes = {
  children: node.isRequired,
};

export default Layout;
