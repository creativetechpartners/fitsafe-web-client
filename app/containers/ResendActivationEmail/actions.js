import { RESEND_ACTIVATION_EMAIL, RESEND_ACTIVATION_EMAIL_FAIL } from './constants';

export const resendActivationEmail = data => ({
  type: RESEND_ACTIVATION_EMAIL,
  data,
});

export const resendActivationEmailFail = error => ({
  type: RESEND_ACTIVATION_EMAIL_FAIL,
  error,
});
