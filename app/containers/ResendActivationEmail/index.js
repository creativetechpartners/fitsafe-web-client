import React from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import { func, array } from 'prop-types';
import Input from 'components/Input';
import { resendActivationEmail } from './actions';
import './style.scss';

const ResendActivationEmailForm = (props) => {
  const { handleSubmit, err } = props;
  return (
    <div className="resend-activation-email">
      <img className="resend-activation-email__logo" src="/img/loginLogo.png" alt="fitsafe logo" />
      <form className="resend-activation-email__form" onSubmit={handleSubmit} autoComplete="off">
        <div className="resend-activation-email__inputs">
          <Field name="email" placeholder="Email" component={Input} type="text" />
          <div className="login__error">{err && err[0]}</div>
        </div>
        <button className="resend-activation-email__btn btn btn-raised btn-info" type="submit">
          Resend email
        </button>
      </form>
    </div>
  );
};

ResendActivationEmailForm.defaultProps = {
  err: undefined,
};

ResendActivationEmailForm.propTypes = {
  handleSubmit: func.isRequired,
  err: array,
};

const wrappedInForm = reduxForm({
  form: 'resend-activation-email',
  onSubmit: (data, dispatch) => dispatch(resendActivationEmail(data)),
})(ResendActivationEmailForm);

export default connect(({ resendEmail }) => ({ err: resendEmail.errors }), null)(wrappedInForm);
