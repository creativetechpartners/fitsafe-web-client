import { takeEvery, put } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import { RESEND_ACTIVATION_EMAIL } from './constants';
import { resendActivationEmailFail } from './actions';
import api from '../../utils/api';

function* resendActivationEmail({ data }) {
  try {
    yield api.post('user/resend_activation-email', { body: data });
    yield put(push('/after_registration'));
  } catch (err) {
    yield put(resendActivationEmailFail(err.errors));
  }
}

export default function* resendActivationEmailSaga() {
  yield* [takeEvery(RESEND_ACTIVATION_EMAIL, resendActivationEmail)];
}
