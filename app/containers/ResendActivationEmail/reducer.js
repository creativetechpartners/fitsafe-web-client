import { combineReducers } from 'redux';
import { RESEND_ACTIVATION_EMAIL_FAIL } from './constants';
import combineActions from '../../utils/combineActions';

const errors = combineActions(
  {
    [RESEND_ACTIVATION_EMAIL_FAIL]: (state, action) => action.error || null,
  },
  null,
);

export default combineReducers({
  errors,
});
