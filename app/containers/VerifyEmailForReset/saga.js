import { takeEvery, put } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import api from '../../utils/api';
import { SUBMIT_EMAIL_FOR_RESET_PASSWORD } from './constants';
import { submitEmailForResetPasswordFailed } from './actions';

function* submitEmail({ data }) {
  try {
    yield api.post('user/check/for_reset', { body: data });
    yield put(push('/after_check_email'));
  } catch (err) {
    yield put(submitEmailForResetPasswordFailed(err.error));
  }
}

export default function* submitEmailForResetSaga() {
  yield* [
    takeEvery(SUBMIT_EMAIL_FOR_RESET_PASSWORD, submitEmail),
  ];
}
