import { SUBMIT_EMAIL_FOR_RESET_PASSWORD, SUBMIT_EMAIL_FOR_RESET_PASSWORD_FAILED } from './constants';

export const submitEmailForResetPassword = data => ({
  type: SUBMIT_EMAIL_FOR_RESET_PASSWORD,
  data,
});

export const submitEmailForResetPasswordFailed = error => ({
  type: SUBMIT_EMAIL_FOR_RESET_PASSWORD_FAILED,
  error,
});

