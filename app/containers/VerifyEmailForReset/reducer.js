import { combineReducers } from 'redux';
import { SUBMIT_EMAIL_FOR_RESET_PASSWORD_FAILED } from './constants';
import combineActions from '../../utils/combineActions';

const verifyEmailForResetError = combineActions({
  [SUBMIT_EMAIL_FOR_RESET_PASSWORD_FAILED]: (state, { error }) => error,
}, {});

export default combineReducers({
  verifyEmailForResetError,
});
