import React from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import { func, string } from 'prop-types';
import Input from 'components/Input';
import { submitEmailForResetPassword } from './actions';
import './style.scss';

const VerifyEmailForReset = (props) => {
  const { handleSubmit, err } = props;
  return (
    <div className="reset-pass">
      <img className="reset-pass__logo" src="/img/loginLogo.png" alt="fitsafe logo" />
      <form className="reset-pass__form" onSubmit={handleSubmit}>
        <Field name="email" placeholder="Email" component={Input} type="email" error={err} />
        <button type="submit" className="reset-pass__btn btn btn-raised btn-info">
          Reset
        </button>
      </form>
    </div>
  );
};

VerifyEmailForReset.defaultProps = {
  err: undefined,
};

VerifyEmailForReset.propTypes = {
  handleSubmit: func.isRequired,
  err: string,
};

const wrappedInForm = reduxForm({
  form: 'reset',
  onSubmit: (data, dispatch) => dispatch(submitEmailForResetPassword(data)),
})(VerifyEmailForReset);

export default connect(({ verifyEmailForReset }) => ({
  err: verifyEmailForReset.verifyEmailForResetError.message,
}))(wrappedInForm);
