import React from 'react';
import './style.scss';

const AfterVerifyEmail = () => (
  <div>
    <h1 className="after-verify-msg">Verification mail for reset password has been sent to your e-mail.</h1>
  </div>
);

export default AfterVerifyEmail;
