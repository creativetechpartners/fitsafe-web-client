import { takeEvery, put } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import api from '../../utils/api';
import { ALLOW_RESET_PASSWORD, RESET_PASSWORD } from './constants';
import { resetNotAllowed } from './actions';

function* letResetPassword({ token }) {
  try {
    yield api.get(`user/let_reset_password/${token}`);
  } catch (err) {
    yield put(resetNotAllowed());
    // eslint-disable-next-line no-console
    console.log(err);
  }
}

function* resetPassword({ data, token }) {
  try {
    yield api.put(`user/reset_password/${token}`, { body: data });
    yield put(push('/login'));
  } catch (err) {
    // eslint-disable-next-line no-console
    console.log(err);
  }
}

export default function* resetPasswordSaga() {
  yield* [
    takeEvery(ALLOW_RESET_PASSWORD, letResetPassword),
    takeEvery(RESET_PASSWORD, resetPassword),
  ];
}
