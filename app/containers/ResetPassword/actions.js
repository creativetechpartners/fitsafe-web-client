import {
  ALLOW_RESET_PASSWORD,
  RESET_PASSWORD_NOT_ALLOWED,
  RESET_PASSWORD,
} from './constants';

export const letReset = token => ({
  type: ALLOW_RESET_PASSWORD,
  token,
});

export const resetNotAllowed = () => ({
  type: RESET_PASSWORD_NOT_ALLOWED,
  status: true,
});

export const resetPassword = (data, token) => ({
  type: RESET_PASSWORD,
  data,
  token,
});

