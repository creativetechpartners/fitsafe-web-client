import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Input from 'components/Input';
import { func, object, bool } from 'prop-types';
import { letReset, resetPassword } from './actions';
import './style.scss';

class ResetPasswordForm extends Component {
  componentDidMount() {
    this.props.letReset(this.props.params.token);
  }
  render() {
    const { handleSubmit } = this.props;
    const notAllowed = this.props.notAllowed;
    return (
      <div className="reset-pass">
        <img className="reset-pass__logo" src="/img/loginLogo.png" alt="fitsafe logo" />
        {!notAllowed ?
          <form className="reset-pass__form" onSubmit={handleSubmit}>
            <Field
              name="newPassword"
              placeholder="New password"
              component={Input}
              type="password"
            />
            <button type="submit" className="reset-pass__btn btn btn-raised btn-info">Reset</button>
          </form> :
          <h1>You did not pass verification by email</h1>
        }
      </div>
    );
  }
}

ResetPasswordForm.propTypes = {
  params: object.isRequired,
  letReset: func.isRequired,
  handleSubmit: func.isRequired,
  notAllowed: bool.isRequired,
};

const wrappedInForm = reduxForm({
  form: 'reset_password',
  onSubmit: (data, dispatch, { params: { token } }) => dispatch(resetPassword(data, token)),
})(ResetPasswordForm);

export default connect(
  ({ resetNotAllowed }) => ({ notAllowed: resetNotAllowed }),
  dispatch => bindActionCreators({
    letReset,
  }, dispatch),
)(wrappedInForm);
