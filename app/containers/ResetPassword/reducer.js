import combineActions from '../../utils/combineActions';
import { RESET_PASSWORD_NOT_ALLOWED } from './constants';

const resetNotAllowed = combineActions({
  [RESET_PASSWORD_NOT_ALLOWED]: (state, action) => action.status,
}, false);

export default resetNotAllowed;

