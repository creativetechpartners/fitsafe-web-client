import { combineReducers } from 'redux';
import {
  USER_LOGIN_FAIL,
  USER_LOGIN_SUCCESS,
  RESTORE_TOKEN,
  LOG_OUT,
  PUT_USER,
  UPDATE_REPORT_LINK,
  REMOVE_REPORT_LINK,
} from './constants';

import combineActions from '../../utils/combineActions';

const loginError = combineActions(
  {
    [USER_LOGIN_SUCCESS]: () => ({}),
    [USER_LOGIN_FAIL]: (state, { error }) => error,
  },
  {},
);

const user = combineActions(
  {
    [USER_LOGIN_SUCCESS]: (state, action) => action.user,
    [UPDATE_REPORT_LINK]: (state, action) => ({ ...state, reportLink: action.reportLink }),
    [PUT_USER]: (state, action) => action.user,
    [REMOVE_REPORT_LINK]: state => ({ ...state, reportLink: '' }),
    [LOG_OUT]: () => null,
  },
  {},
);

const token = combineActions(
  {
    [USER_LOGIN_SUCCESS]: (state, action) => action.token,
    [RESTORE_TOKEN]: (state, action) => action.token,
    [LOG_OUT]: () => null,
  },
  null,
);

export default combineReducers({
  loginError,
  user,
  token,
});
