import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { push } from 'react-router-redux';
import { Field, reduxForm } from 'redux-form';
import { func, string } from 'prop-types';
import Input from 'components/Input';
import { loginUser } from './actions';
import './style.scss';

const LoginForm = (props) => {
  const { handleSubmit } = props;
  return (
    <div className="login">
      <img className="login__logo" src="/img/loginLogo.png" alt="fitsafe logo" />
      <form className="login__form" onSubmit={handleSubmit} autoComplete="off">
        <div className="login__inputs">
          <Field name="email" placeholder="Email" component={Input} type="text" />
          <Field name="password" placeholder="Password" component={Input} type="password" />
          <div className="login__error">{props.err}</div>
        </div>
        <button className="login__btn btn btn-raised btn-info" type="submit">
          Sign in
        </button>
      </form>
      <div className="login__actions">
        <a onClick={() => props.push('/registration')}>Create Account</a>
        <a onClick={() => props.push('/verify_email_for_reset')}>Forgot password?</a>
        <a onClick={() => props.push('/resend_activation_email')}>Resend activation email</a>
      </div>
    </div>
  );
};

LoginForm.defaultProps = {
  err: undefined,
};

LoginForm.propTypes = {
  handleSubmit: func.isRequired,
  err: string,
  push: func.isRequired,
};

const wrappedInForm = reduxForm({
  form: 'login',
  onSubmit: (data, dispatch) => dispatch(loginUser(data)),
})(LoginForm);

export default connect(
  ({ login }) => ({ err: login.loginError.message }),
  dispatch =>
    bindActionCreators(
      {
        push,
      },
      dispatch,
    ),
)(wrappedInForm);
