import { takeEvery, put } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import api, { setToken } from '../../utils/api';
import { USER_LOGIN, LOG_OUT, REMOVE_REPORT_LINK } from './constants';
import {
  loginUserFail,
  authSuccess,
  restoreToken,
  removeReportLinkFail,
  removeReportLinkSuccess,
} from './actions';

function* login({ data }) {
  try {
    const { user, token } = yield api.post('user/login', { body: data });
    window.localStorage.setItem('token', token);
    setToken(token);
    yield put(authSuccess(user, token));
    yield put(push('/'));
  } catch (err) {
    yield put(loginUserFail(err.error));
  }
}

function* logOut() {
  window.localStorage.clear();
  yield put(push('/login'));
}

function* removeReportLink() {
  try {
    const { user } = yield api.post('user/resetReportLink');
    yield put(removeReportLinkSuccess(user));
  } catch (err) {
    console.error(err);
    yield put(removeReportLinkFail(err.error));
  }
}

export default function* loginSaga() {
  const token = window.localStorage.getItem('token');
  if (token) {
    setToken(token);
    yield put(restoreToken(token));
  }
  yield* [
    takeEvery(USER_LOGIN, login),
    takeEvery(REMOVE_REPORT_LINK, removeReportLink),
    takeEvery(LOG_OUT, logOut),
  ];
}
