import {
  USER_LOGIN,
  USER_LOGIN_FAIL,
  USER_LOGIN_SUCCESS,
  RESTORE_TOKEN,
  LOG_OUT,
  PUT_USER,
  UPDATE_REPORT_LINK,
  REMOVE_REPORT_LINK,
  REMOVE_REPORT_LINK_SUCCESS,
  REMOVE_REPORT_LINK_FAIL,
} from './constants';

export const loginUser = data => ({
  type: USER_LOGIN,
  data,
});

export const loginUserFail = error => ({
  type: USER_LOGIN_FAIL,
  error,
});

export const authSuccess = (user, token) => ({
  type: USER_LOGIN_SUCCESS,
  user,
  token,
});

export const putUser = user => ({
  type: PUT_USER,
  user,
});

export const removeReportLink = () => ({
  type: REMOVE_REPORT_LINK,
});

export const removeReportLinkSuccess = user => ({
  type: REMOVE_REPORT_LINK_SUCCESS,
  user,
});

export const removeReportLinkFail = error => ({
  type: REMOVE_REPORT_LINK_FAIL,
  error,
});

export const updateReportLink = reportLink => ({
  type: UPDATE_REPORT_LINK,
  reportLink,
});

export const restoreToken = token => ({
  type: RESTORE_TOKEN,
  token,
});

export const logOut = () => ({
  type: LOG_OUT,
});
