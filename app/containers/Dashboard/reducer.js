import { combineReducers } from 'redux';
import combineActions from 'utils/combineActions';
import { CATEGORIES_LOADED, REPORT_GENERATION_FAILED, SUBMIT_SCORING } from './constants';

const categories = combineActions(
  {
    [CATEGORIES_LOADED]: (state, action) => action.categories,
  },
  {},
);

const thankYouLoading = combineActions(
  {
    [SUBMIT_SCORING]: () => true,
  },
  true,
);

const reportGenerationError = combineActions(
  {
    [REPORT_GENERATION_FAILED]: () => true,
  },
  false,
);

export default combineReducers({
  categories,
  thankYouLoading,
  reportGenerationError,
});
