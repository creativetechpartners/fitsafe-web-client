import {
  LOAD_CATEGORIES,
  CATEGORIES_LOADED,
  SUBMIT_SCORING,
  SCORING_LOADED,
  CATEGORIES_SURVEYS_LOADED,
  REPORT_GENERATION_FAILED,
} from './constants';

export const loadCategories = () => ({
  type: LOAD_CATEGORIES,
});

export const categoriesLoaded = categories => ({
  type: CATEGORIES_LOADED,
  categories,
});

export const submitScoring = () => ({
  type: SUBMIT_SCORING,
});

export const reportGenerationFailed = () => ({
  type: REPORT_GENERATION_FAILED,
});

export const scoringLoaded = ({ scoring }) => ({
  type: SCORING_LOADED,
  scoring,
});

export const categoriesSurveysLoaded = categoriesSurveys => ({
  type: CATEGORIES_SURVEYS_LOADED,
  categoriesSurveys,
});
