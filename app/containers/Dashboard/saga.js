import { takeEvery, put, select } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import api from '../../utils/api';
import { LOAD_CATEGORIES, SUBMIT_SCORING, CATEGORIES_LOADED } from './constants';
import { USER_LOGIN_SUCCESS } from '../../containers/Login/constants';
import {
  categoriesLoaded,
  reportGenerationFailed,
  scoringLoaded,
  categoriesSurveysLoaded,
} from './actions';
import { updateReportLink } from '../Login/actions';

const token = window.localStorage.getItem('token');

function* loadCategories() {
  const { categories } = yield api.get('category');
  const { report } = yield api.get('scoring');
  if (report) {
    yield put(scoringLoaded(report));
  }
  yield put(categoriesLoaded(categories));
}

function* submitScoring() {
  try {
    const { scoring, categoriesScores, attachments } = yield select(({ category }) => category);

    const { categories } = yield select(({ dashboard }) => dashboard);

    yield put(push('/thank-you'));

    const { link } = yield api.post('report', {
      body: {
        scoring,
        categoriesScores,
        attachments,
        categories,
      },
    });

    yield put(updateReportLink(link));
    yield put(push('/dashboard'));
  } catch (err) {
    // eslint-disable-next-line no-console
    yield put(reportGenerationFailed());

    console.log(err);
  }
}

function* loadCategoriesSurveys({ categories }) {
  try {
    const categoriesSurveys = yield Promise.all(
      Object.keys(categories).map(category =>
        api.get('survey', {
          query: {
            category,
          },
        }),
      ),
    );

    const transformedSurveys = categoriesSurveys.reduce(
      (acc, obj) => ({ ...acc, ...obj.surveys }),
      {},
    );

    yield put(categoriesSurveysLoaded(transformedSurveys));
  } catch (err) {
    console.log(err);
  }
}

export default function* dashboardSaga() {
  yield* [
    takeEvery(LOAD_CATEGORIES, loadCategories),
    takeEvery(USER_LOGIN_SUCCESS, loadCategories),
    takeEvery(SUBMIT_SCORING, submitScoring),
    takeEvery(CATEGORIES_LOADED, loadCategoriesSurveys),
  ];
  if (token) {
    yield loadCategories();
  }
}
