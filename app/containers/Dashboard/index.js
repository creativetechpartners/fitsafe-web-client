import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import _ from 'lodash';
import { func, object, string } from 'prop-types';
import Date from 'components/Date';
import CategoryCard from 'components/CategoryCard';
import AverageScoreCard from 'components/AverageScoreCard';
import AlertModal from 'components/AlertModal';
// import Location from 'components/Location';
// import Contacts from 'components/Contacts';
// import StaffMembers from 'components/StaffMembers';
import { submitScoring } from './actions';
import { resetAll } from '../Category/actions';
import { logOut, removeReportLink } from '../Login/actions';
import './styles.scss';
import { Button } from 'react-bootstrap';

class Dashboard extends Component {
  state = {
    alertModalOpened: false,
    alertResetModalOpened: false,
    listOfUnansweredQuestionsIds: null,
  };
  getAnswersQuantity(answers = {}) {
    return Object.keys(answers).reduce(
      (acc, surveyId) => Object.keys(_.get(answers, `[${surveyId}].answers`, {})).length + acc,
      0,
    );
  }

  getAllAnswersQuantity(scoring, categoriesKeys) {
    return categoriesKeys.reduce((acc, key) => {
      return this.getAnswersQuantity(scoring[key]) + acc;
    }, 0);
  }

  getCategoryScoring = ({ possiblePoints }, categoriesScore = {}, surveys = []) => {
    return Object.keys(categoriesScore).reduce(
      ({ score, maxScore }, questionId) => {
        return {
          score: score + categoriesScore[questionId],
          maxScore: maxScore + possiblePoints[questionId],
          showScore: surveys.some(s => s.significant),
        };
      },
      { score: 0, maxScore: 0, showScore: false },
    );
  };

  getAvrScoring = (categories, categoriesScores) => {
    return Object.keys(categories).reduce(
      (acc, categoryId) => {
        const { score, maxScore } = this.getCategoryScoring(
          categories[categoryId],
          categoriesScores[categoryId],
        );

        return {
          score: acc.score + score,
          maxScore: acc.maxScore + maxScore,
        };
      },
      { score: 0, maxScore: 0 },
    );
  };

  submitScoring = () => {
    const { surveys, scoring, categories } = this.props;
    const unAnsweredSurveysIdsStruct = Object.keys(categories).reduce(
      (acc, categoryId) => ({
        ...acc,
        [categoryId]: !scoring[categoryId]
          ? Object.keys(surveys).filter(surveyId => surveys[surveyId].category === categoryId)
          : Object.keys(surveys)
              .filter(surveyId => surveys[surveyId].category === categoryId)
              .reduce(
                (acc2, surveyId) =>
                  typeof _.get(scoring, `[${categoryId}][${surveyId}].mainQuestion`) === 'undefined'
                    ? [...acc2, surveyId]
                    : acc2,
                [],
              ),
      }),
      [],
    );

    const unAnsweredSurveysIds = Object.values(unAnsweredSurveysIdsStruct).reduce(
      (acc, surveys) => [...acc, ...surveys],
      [],
    );

    const haveUnansweredQuestions = !!unAnsweredSurveysIds.length;

    if (haveUnansweredQuestions) {
      this.setState({
        alertModalOpened: true,
        listOfUnansweredQuestions: unAnsweredSurveysIdsStruct,
      });
      return;
    }

    this.props.submitScoring();
  };

  resetAll = async () => {
    this.props.resetAllCategories();
    this.props.removeReportLink();
    this.setState({ alertResetModalOpened: false });
  };

  get errorsList() {
    const { listOfUnansweredQuestions } = this.state;
    const { categories, surveys } = this.props;

    if (!listOfUnansweredQuestions) return '';

    const errors = Object.keys(listOfUnansweredQuestions).reduce(
      (acc, categoryId) => ({
        ...acc,
        [categories[categoryId].name]: listOfUnansweredQuestions[categoryId].map(
          surveyId => surveys[surveyId].name,
        ),
      }),
      {},
    );

    const filteredErrors = Object.keys(errors).reduce(
      (acc, categoryName) =>
        Object.keys(errors[categoryName]).length
          ? { ...acc, [categoryName]: errors[categoryName] }
          : acc,
      {},
    );

    return (
      <div>
        {Object.keys(filteredErrors).map(categoryName => (
          <div key={categoryName}>
            <div>{categoryName}</div>
            <ul>
              {errors[categoryName].map(surveyName => (
                <li key={surveyName}>{surveyName}</li>
              ))}
            </ul>
          </div>
        ))}
      </div>
    );
  }

  render() {
    const { categories, scoring, goTo, categoriesScores, surveys } = this.props;
    const reportLink = this.props.reportLink || '';
    const categoriesKeys = Object.keys(categories);
    return (
      <div className="dashboard">
        <AlertModal
          show={this.state.alertModalOpened}
          onHide={() => this.setState({ alertModalOpened: false, listOfUnansweredQuestions: null })}
          title="You can't submit a report because you haven't answered questions in the following categories:"
          content={
            <div>
              {this.errorsList}
              <Button
                onClick={() =>
                  this.setState({ alertModalOpened: false, listOfUnansweredQuestions: null })
                }
                bsStyle="info"
                className="btn-raised pull-right"
              >
                OK
              </Button>
            </div>
          }
        />
        <AlertModal
          show={this.state.alertResetModalOpened}
          onHide={() => this.setState({ alertResetModalOpened: false })}
          title="Your report has been submitted. All sections are locked. Would you like to clear all sections to stat again?"
          content={
            <div className="text-right">
              <Button
                onClick={() => this.setState({ alertResetModalOpened: false })}
                style={{ marginRight: '20px' }}
                className="btn-raised"
              >
                Close
              </Button>
              <Button onClick={this.resetAll} bsStyle="info" className="btn-raised">
                Clear All
              </Button>
            </div>
          }
        />
        <Date />
        <div className="divider" />
        {/* <div>
          <p className="profileTitle">Fitness Center Profile</p>
          <div className="profileDetailsContainer">
            <Location className="dashboardLocation" />
            <div className="vdivider" />
            <Contacts className="dashboardContacts" />
            <div className="vdivider" />
            <StaffMembers className="dashboardStaffMembers" />
          </div>
          <div className="divider" />
        </div> */}

        {!!categoriesKeys.length && (
          <div className="mainCategoriesContainer">
            {!reportLink.length ? (
              <p className="areas">
                Before your assessment, please be sure to have a (A) light meter, (B) sound meter,
                and (C) 50-foot tape measure handy.
              </p>
            ) : (
              <div className="submittedReport">
                <h2>Thank you for submitting your report!</h2>
                <h3>It was sent to your email address.</h3>
                <p>Please check your inbox or review it by clicking download report button</p>
              </div>
            )}
            <div className="categoriesContainer">
              {categoriesKeys.map(categoryId => (
                <CategoryCard
                  key={categoryId}
                  category={categories[categoryId]}
                  answersQuantity={this.getAnswersQuantity(scoring[categoryId])}
                  goTo={
                    !reportLink.length ? goTo : () => this.setState({ alertResetModalOpened: true })
                  }
                  {...this.getCategoryScoring(
                    categories[categoryId],
                    categoriesScores[categoryId],
                    Object.keys(surveys)
                      .filter(k => surveys[k].category === categoryId)
                      .map(k => surveys[k]),
                  )}
                />
              ))}
              <AverageScoreCard
                reportLink={reportLink}
                submitScoring={this.submitScoring}
                answersQuantity={this.getAllAnswersQuantity(scoring, categoriesKeys)}
                {...this.getAvrScoring(categories, categoriesScores)}
              />
            </div>
          </div>
        )}
      </div>
    );
  }
}

Dashboard.propTypes = {
  categories: object.isRequired,
  scoring: object.isRequired,
  goTo: func.isRequired,
  removeReportLink: func.isRequired,
  categoriesScores: object.isRequired,
  submitScoring: func.isRequired,
  resetAllCategories: func.isRequired,
  reportLink: string.isRequired,
  surveys: object.isRequired,
};

export default connect(
  ({
    dashboard: { categories },
    category: { scoring, categoriesScores, surveys },
    login: {
      user: { reportLink },
    },
  }) => ({
    reportLink,
    categories,
    scoring,
    categoriesScores,
    surveys,
  }),
  dispatch =>
    bindActionCreators(
      {
        goTo: push,
        submitScoring,
        resetAllCategories: resetAll,
        goOut: logOut,
        removeReportLink,
      },
      dispatch,
    ),
)(Dashboard);
