import { USER_ACTIVATE, USER_ACTIVATE_FAILED, USER_ACTIVATE_SUCCESS } from './constants';

export const activateUser = token => ({
  type: USER_ACTIVATE,
  token,
});

export const activateUserFailed = error => ({
  type: USER_ACTIVATE_FAILED,
  status: false,
  error,
});

export const activateUserSuccess = () => ({
  type: USER_ACTIVATE_SUCCESS,
  status: true,
});
