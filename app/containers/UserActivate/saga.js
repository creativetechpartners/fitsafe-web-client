import { takeEvery, put } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import api from '../../utils/api';
import { USER_ACTIVATE } from './constants';
import { activateUserFailed, activateUserSuccess } from './actions';

const timeStub = time => new Promise(ok => setTimeout(ok, time));

function* activation({ token }) {
  try {
    yield api.put(`user/activation/${token}`);
    yield put(activateUserSuccess());

    yield timeStub(2500);
    yield put(push('/dashboard'));
  } catch (err) {
    yield put(activateUserFailed(err.error));
  }
}

export default function* activationSaga() {
  yield* [takeEvery(USER_ACTIVATE, activation)];
}
