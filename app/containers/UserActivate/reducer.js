import combineActions from '../../utils/combineActions';
import { USER_ACTIVATE_FAILED, USER_ACTIVATE_SUCCESS } from './constants';

const errorActivate = combineActions(
  {
    [USER_ACTIVATE_FAILED]: (state, action) => ({ status: action.status, error: action.error }),
    [USER_ACTIVATE_SUCCESS]: (state, action) => ({ status: action.status }),
  },
  {},
);

export default errorActivate;
