import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { func, object, bool } from 'prop-types';
import { activateUser } from './actions';
import './style.scss';

class UserActivate extends Component {
  componentDidMount() {
    this.props.activateUser(this.props.params.token);
  }

  renderComponent = message => <h1>{message}</h1>;

  render() {
    const { status, error = {} } = this.props.error;

    return (
      <div className="user-activate">
        {typeof status === 'undefined' && this.renderComponent('Loading...')}
        {status === false && this.renderComponent(error.message || 'Invalid token...')}
        {status === true &&
          this.renderComponent(
            'You have successfully confirmed your mail. Now you will be redirected to the login page',
          )}
      </div>
    );
  }
}

UserActivate.propTypes = {
  params: object.isRequired,
  activateUser: func.isRequired,
  error: object.isRequired,
};

export default connect(
  ({ activateError }) => ({ error: activateError }),
  dispatch =>
    bindActionCreators(
      {
        activateUser,
      },
      dispatch,
    ),
)(UserActivate);
