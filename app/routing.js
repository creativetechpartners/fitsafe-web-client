import React from 'react';
import Layout from 'containers/Layout';
// import Categories from 'containers/Categories';
import RegistrationForm from 'containers/Registration';
import LoginForm from 'containers/Login';
import Dashboard from 'containers/Dashboard';
import NotFound from 'containers/NotFound';
import { Router, Route } from 'react-router';
import Category from 'containers/Category';
import UserActivate from 'containers/UserActivate';
import AccessRoute from 'components/AccessRoute';
import AfterRegistration from 'containers/AfterRegistration';
import Payment from 'containers/Payment';
import PaymentError from 'containers/PaymentError';
import VerifyEmailForReset from 'containers/VerifyEmailForReset';
import AfterVerifyEmail from 'containers/AfterVerifyEmail';
import ResetPassword from 'containers/ResetPassword';
import ResendActivationEmail from 'containers/ResendActivationEmail';
import ThankYou from 'containers/ThankYou';
import { object } from 'prop-types';
import get from 'lodash/get';

const loggedIn = ({ login: { token } }) => !!token;
const isPaid = ({ login: { user } }) => get(user, 'isPaid', false);

const MainRouter = (props) => {
  const { store } = props;
  return (
    <Router {...props}>
      <Route component={Layout}>
        <AccessRoute store={store} check={() => !loggedIn(store.getState())} redirect="/">
          <Route path="/registration" component={RegistrationForm} />
          <Route path="/after_registration" component={AfterRegistration} />
          {/* <Route path="/after_login" component={AfterLogin} /> */}
          <Route path="/activation/:token" component={UserActivate} />
          <Route path="/login" component={LoginForm} />
          <Route path="/verify_email_for_reset" component={VerifyEmailForReset} />
          <Route path="/after_check_email" component={AfterVerifyEmail} />
          <Route path="/reset_password/:token" component={ResetPassword} />
          <Route path="resend_activation_email" component={ResendActivationEmail} />
        </AccessRoute>

        <AccessRoute store={store} check={() => loggedIn(store.getState())} redirect="/login">
          <AccessRoute store={store} check={() => !isPaid(store.getState())} redirect="/dashboard">
            <Route path="/" component={Payment} />
            <Route path="/payment-error" component={PaymentError} />
          </AccessRoute>

          <AccessRoute store={store} check={() => isPaid(store.getState())} redirect="/">
            <Route path="/dashboard" component={Dashboard} />
            <Route path="category/:category" component={Category} />
            <Route path="/thank-you" component={ThankYou} />
          </AccessRoute>
        </AccessRoute>
        <Route path="*" component={NotFound} />
      </Route>
    </Router>
  );
};

MainRouter.propTypes = {
  store: object.isRequired,
};

export default MainRouter;
