import _ from 'lodash';
import global from './global';
import local from './local';

export default _.merge({}, global, local);
