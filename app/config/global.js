export default {
  hotLoader: {
    host: 'localhost',
    port: 9000,
  },
  host: 'localhost',
  port: 9001,
  apiHost: 'http://www.devteam-test.tk',
};
