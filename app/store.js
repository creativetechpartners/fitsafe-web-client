import { createStore, applyMiddleware, compose } from 'redux';
import { routerMiddleware as createRouterMiddleware } from 'react-router-redux';
import persistState from 'redux-localstorage';
import createSagaMiddleware from 'redux-saga';
import createReducer from 'reducers';
import rootSaga from 'sagas';

const persistSlicer = () => ({ login: { user } }) => ({
  login: {
    user,
  },
});

export default function configureStore(history, initialState = {}) {
  const sagaMiddleware = createSagaMiddleware();

  const middlewares = [
    sagaMiddleware,
    createRouterMiddleware(history),
  ];

  const enhancers = [
    applyMiddleware(...middlewares),
    persistState(undefined, { key: 'fitsafe-store', slicer: persistSlicer }),
  ];

  // If Redux DevTools Extension is installed use it, otherwise use Redux compose
    /* eslint-disable no-underscore-dangle */
  const composeEnhancers =
      process.env.NODE_ENV !== 'production' &&
      typeof window === 'object' &&
      window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
        window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ : compose;
    /* eslint-enable */

  const store = createStore(
    createReducer(),
    initialState,
    composeEnhancers(...enhancers),
  );

  sagaMiddleware.run(rootSaga);

  return store;
}
