import Webpack from 'webpack';
import path from 'path';
import config from '../../app/config';

const HotModuleReplacementPlugin = new Webpack.HotModuleReplacementPlugin();

const {
  hotLoader: {
    host: hotHost,
    port: hotPort,
  },
} = config;

export default {
  devtool: 'source-map',
  entry: [
    `webpack-dev-server/client?http://${hotHost}:${hotPort}`,
    'webpack/hot/dev-server',
    path.join(__dirname, '../../app/root.js'),
  ],
  output: {
    path: path.join(__dirname, '../../static/'),
    filename: 'bundle.js',
    publicPath: '/',
  },
  module: {
    loaders: [
      {
        test: /\.js/,
        exclude: /node_modules/,
        loader: 'babel-loader?cacheDirectory',
      }, {
        test: /\.json$/,
        loader: 'json-loader',
      },
      {
        test: /\.(scss|sass|css)$/,
        loader: 'style-loader!css-loader!sass-loader?cacheDirectory',
      },
      {
        test: /\.(eot|svg|ttf|woff|woff2)$/,
        loader: 'file-loader',
      },
      {
        test: /\.(jpg|png|gif)$/,
        loaders: [
          'file-loader',
          {
            loader: 'image-webpack-loader',
            query: {
              mozjpeg: {
                progressive: true,
              },
              gifsicle: {
                interlaced: false,
              },
              optipng: {
                optimizationLevel: 7,
              },
              pngquant: {
                quality: '65-90',
                speed: 4,
              },
            },
          },
        ],
      },
    ],
  },
  resolve: {
    modules: [
      'app',
      'node_modules',
    ],
  },
  plugins: [
    HotModuleReplacementPlugin,
  ],
};
